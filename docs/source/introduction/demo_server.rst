.. _demo-server:

Demonstration server
====================

We provide a *demo server* in the ``accounts/demo`` directory. It makes it easier to test the application without loosing time to configure Django.

1. Create a Python virtual environment: ``python3 -m venv ./venv``.
2. Activate virtual environment: ``source ./venv/bin/activate``.
3. Install the application requirements: ``pip install -r requirements.txt``.
4. Go to the directory: ``cd accounts/demo``.
5. Create the database and apply migrations: ``./manage.py migrate``.
6. Create a super user: ``./manage.py createsuperuser --username root --email root@localhost``
7. Start the server: ``./manage.py runserver 5000``.

Or, all in one:

.. code-block:: bash

    git clone https://github.com/guilieb/django-gaccounts
    cd django-gaccounts
    python3 -m venv ./venv
    source ./venv/bin/activate
    pip install -r requirements.txt
    cd accounts/demo
    ./manage.py migrate
    ./manage.py createsuperuser --username root --email root@localhost
    ./manage.py runserver 5000


Now, go to https://localhost:5000/accounts/ and log in using the credentials provided for root user.
