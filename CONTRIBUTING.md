# Contribution guide

Please, see [The Koala LMS Contribution Guide (on `master`)](https://gitlab.com/koala-lms/lms/-/blob/master/CONTRIBUTING.md) or  [The Koala LMS Contribution Guide (on `develop`)](https://gitlab.com/koala-lms/lms/-/blob/develop/CONTRIBUTING.md)
