#
# Copyright (C) 2019 Guillaume Bernard <guillaume.bernard@koala-lms.org>
#
# This file is part of Koala LMS (Learning Management system)

# Koala LMS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# We make an extensive use of the Django framework, https://www.djangoproject.com/

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView as AuthLoginView, PasswordChangeView
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import UpdateView, CreateView

from accounts.models import Person, Notification
from .forms import UserCreationForm, UserChangeForm, UserLoginForm


def _update_valid_or_invalid_form_fields(form):
    for field in form.fields:
        try:
            current_class = form.fields[field].widget.attrs['class']
        except KeyError:
            current_class = str()

        if field in form.errors:
            form.fields[field].widget.attrs.update({'class': current_class + ' ' + 'is-invalid'})
        elif field in form.changed_data:
            form.fields[field].widget.attrs.update({'class': current_class + ' ' + 'is-valid'})
    return form


class LoginView(AuthLoginView):
    form_class = UserLoginForm
    redirect_authenticated_user = True

    def form_invalid(self, form):
        messages.error(self.request, _("Login unsuccessful. Please verify your username and password."))
        return super().form_invalid(form)


class PasswordChange(PasswordChangeView):
    success_url = reverse_lazy('accounts:details')
    title = _("My account − Change password")

    def form_valid(self, form):
        messages.success(self.request, _("Password updated!"))
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, _("Error when updating password. You should fix the issues and try again."))
        form = _update_valid_or_invalid_form_fields(form)
        return super().form_invalid(form)


class AccountsRegisterView(CreateView):
    template_name = "accounts/register.html"
    form_class = UserCreationForm
    success_url = reverse_lazy("accounts:details")
    extra_context = {
        "title": _("Create a new account")
    }

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password2')
        user = authenticate(username=username, password=password)
        login(self.request, user)
        messages.success(
            self.request, _("Your registration went correctly. You’re now connected and you can "
                            "update your personal information from this page.")
        )
        return super().form_valid(form)

    def form_invalid(self, form):
        for error in form.errors:
            messages.error(self.request, form.errors[error])
        return super().form_invalid(form)


class AccountsDetailsView(LoginRequiredMixin, UpdateView):
    template_name = "accounts/details.html"
    form_class = UserChangeForm
    success_url = reverse_lazy("accounts:details")

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        form.save()
        if form.has_changed():
            messages.success(self.request, _("Your personal details have been updated!"))
        return super().form_valid(form)

    def form_invalid(self, form):
        form = _update_valid_or_invalid_form_fields(form)
        messages.error(self.request, _("Error when changing personal details. Check errors and retry."))
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("My account − %(username)s") % {'username': self.request.user.display_name}
        return context


@login_required
def search_user(request):
    """
    Searching for a user is limited to − obviously − registered user.
    It seems that this may be a personal data leak, but as any registered
    user can create a course and add collaborator or students, up to now
    there is not other solution.

    :param request:
    :return:
    """
    response = JsonResponse({})
    if request.method == 'GET':
        search_string = request.GET.get('user', None)
        if search_string and len(search_string) > 1:
            persons = Person.objects.filter(
                Q(username__icontains=search_string) |
                Q(first_name__icontains=search_string) |
                Q(last_name__icontains=search_string)
            )
            response = JsonResponse(
                list(persons.values('username', 'first_name', 'last_name')),
                safe=False
            )
    return response


@login_required
def notification_mark_as_read(request, notification_id):
    if request.method == 'POST':
        notification = get_object_or_404(Notification, recipient=request.user, pk=notification_id)
        notification.is_read = True
        notification.save()
    # noinspection PyUnresolvedReferences
    return JsonResponse({'unread': request.user.unread_notifications.count()})


@login_required
def notification_delete(request, notification_id):
    if request.method == 'POST':
        notification = get_object_or_404(Notification, recipient=request.user, pk=notification_id)
        notification.delete()
    # noinspection PyUnresolvedReferences
    return JsonResponse({'unread': request.user.unread_notifications.count()})
