#
# Copyright (C) 2019 Guillaume Bernard <guillaume.bernard@koala-lms.org>
#
# This file is part of Koala LMS (Learning Management system)

# Koala LMS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# We make an extensive use of the Django framework, https://www.djangoproject.com/

from django.contrib.auth.views import PasswordResetView
from django.urls import path, include

from accounts.forms import UserPasswordResetForm
from accounts import views

app_name = 'accounts'

ajax_api_urlpatterns = [
    path('search/', views.search_user, name='ajax/search'),
    path('notification/read/<int:notification_id>', views.notification_mark_as_read, name="ajax/notification/read"),
    path('notification/delete/<int:notification_id>', views.notification_delete, name="ajax/notification/delete"),
]

urlpatterns = [
    path('', views.LoginView.as_view(), name='index'),

    path('login/', views.LoginView.as_view(), name='login'),
    path('register/', views.AccountsRegisterView.as_view(), name='register'),

    path('password_change/', views.PasswordChange.as_view(), name='password_change'),
    path('details/', views.AccountsDetailsView.as_view(), name='details'),

    path('password_reset/', PasswordResetView.as_view(form_class=UserPasswordResetForm), name="password_reset"),

    path('ajax/', include(ajax_api_urlpatterns)),
]
